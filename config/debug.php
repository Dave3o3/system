<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <title>Debugging Informations</title>
  <link rel="stylesheet" type="text/css" href="../config/css/main.css">
  <link rel="stylesheet" href="../config/bower_components/foundation-icon-fonts/foundation-icons.css" media="screen" title="no title" charset="utf-8">
  <style type="text/css">

    .debug-button {
      padding: 15px 20px; background-color: #ccc; color: black; margin-left: 10px;
    }

    table {
      margin-top: 60px;
      color: black;
      table-layout: fixed;
      width: 100%;
    }

    .instruction__title {
      text-align: center;
    }

    table td {
      word-wrap: break-word;
      overflow-wrap: break-word;
    }

    table th {
      text-align: left;
    }

    table th:first-child {
      width: 33%;
    }

  </style>
</head>
<body>

  <header class="expanded row">
    <div class="small-12 columns site__title">
      <div class="row">
        <a href="/config">
          <i class="fi-home"></i>
        </a>
        <img src="assets/glancr_logo.png" width="57" height="30" alt="GLANCR Logo" srcset="assets/glancr_logo.png 57w, assets/glancr_logo@2x.png 114w, assets/glancr_logo@2x.png 171w">
        <a href="../wlanconfig/">
          <i class="fi-widget"></i>
        </a>
      </div>
    </div>
  </header>

  <main class="container">
    <div class="small-12 columns">
      <h2 class="instruction__title">Debugging Informations</h2>

        <?php
          $servername = "localhost";
          $username = "glancr";
          $password = "glancr";
          $dbname = "glancr";
          $keys = array();
          @$key_compare = $_GET['key'];

          $conn = new mysqli($servername, $username, $password, $dbname);

          if ($result = $conn->query('SELECT * FROM configuration;')) {

            echo "<table>";
            echo "<tr><th>Key</th><th>Value</th></tr>";

            while ($row = $result->fetch_assoc()) {
              $parts = explode("_", $row["key"]);

              if (sizeof($parts) >= 2) {
                array_push($keys, $parts[0]);
              }

              if ($key_compare == null || $key_compare == $parts[0]) {
                echo "<tr><td>". $row["key"] . "</td><td>". $row["value"]. "</td></tr>";
              }
            }
            $result->free();

            $keys = array_unique($keys);

            echo "<div class='dubug-buttons'>";
            echo '<a href="/config/debug.php" class="debug-button">Alle</a>';

            foreach ($keys as $key => $value) {
              echo '<a href="/config/debug.php?key='.$value.'" class="debug-button">'.$value.'</a>';
            }
            echo "</div>";
            echo "</table>";

          }
        ?>
    </div>
  </main>
</body>
</html>
