��    B      ,  Y   <      �     �     �  %   �  =   �     "     5  ;   G     �     �     �     �     �     �  	   �  
   �     �       )     %   ?     e  ,   j     �  
   �     �     �     �     �     �            
        $  
   *     5     8     G     d     t     �     �     �     �     �     �     �     	     	     2	     G	     f	     u	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     %
     .
     ?
     P
    c
     s     �  8   �  B   �     .     I  ]   d     �  (   �       	   #     -     =  	   T  	   ^  %   h     �  .   �  )   �  	   �  0   �      +  <   L  $   �  �   �     [  9   i     �     �     �     �     �     �     �     �  >        O     c     t      �     �     �     �     �     �     	     '     F  $   c     �    �     �     �     �     �     �          !     0     =     Q     m  M   r  l   �     -            ,   6      1   &   "         	   !   
       :   %       *   #   3   0              B   2             4   (              .                                   5   9      -         A   )      7   $                          '       ;          =      ?         >   @   +         8      /                  <                 %s: update to version %s 404 There was an error updating mirr.OS:  There's a system update available for your Glancr: Version %s Update modules now Update system now We made some changes. The following modules can be updated: add new module all updates were successful! back to setup cancel choose wlan config module's width connected connecting connecting to wlan delete do you really want to update all modules? do you really want to update mirr.OS? done downloading and installing system update ... email again hint email hint email not sent email not sent text ethernet factory setting text factory settings failed find modules full width go on half width hi input password insert cable and reload site install modules invisible ssid manage modules mirr.OS sucessfully updated module deleted module overview module zip file not a zip file really delete module really overwrite module really overwrite module? really update module rescan available WiFi networks setup complete setup complete text setup step 1 setup step 2 sorry step 1 of 2 step 2 of 2 there was a server error:  visible ssid what is your first name? where are you living? who are you? wireless wlan config text wlan failed text your email address Project-Id-Version: mirr.OS 0.5.7
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-01-18 12:26+0100
PO-Revision-Date: 2017-01-18 12:27+0100
Last-Translator: Tobias Grasse <dotdotfx@gmail.com>
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ../../..
X-Poedit-KeywordsList: _
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: modules
 %s: Aktualisiere auf Version %s 404 - Webseite nicht gefunden! Es gab einen Fehler bei der Aktualisierung von mirr.OS:  Es ist ein System-Update für deinen Glancr verfügbar: Version %s Module jetzt aktualisieren System jetzt aktualisieren Wir haben ein paar Änderungen vorgenommen. Die folgenden Module können aktualisiert werden: Neues Modul hinzufügen Alle Aktualisierungen waren erfolgreich! Zurück zur Konfigurationsseite abbrechen WLAN auswählen Modulbreite einstellen Verbunden Verbinden Verbinde mirr.OS mit deinem Netzwerk. löschen Willst du wirkliche alle Module aktualisieren? Willst du mirr.OS wirklich aktualisieren? Erledigt! System-Update herunterladen und installieren … Bitte E-Mail-Adresse bestätigen Hier senden wir die Zugangslinks für die Konfiguration hin. E-Mail konnte nicht gesendet werden. Es gibt ein Problem mit dem versenden von E-Mails. Um das Problem zu lösen erstellt mirr.OS den AP (Access Point) neu. Bitte konfiguriere anschließend deinen Spiegel neu. Netzwerkkabel Dein glancr wird nun auf Werkseinstellung zurückgesetzt. Werkseinstellung Irgendwas stimmt nicht. Module finden Ganze Breite weiter Halbe Breite Hi Passwort eingeben Bitte verbinde mit einem Netzwerkakbel und lade die Seite neu. Module installieren Unsichtbare SSID Module verwalten mirr.OS erfolgreich aktualisiert Modul wurde gelöscht Modulübersicht ZIP-Datei des Moduls keine ZIP-Datei Modul wirklich löschen Modul wirklich überschreiben Modul wirklich überschreiben? Modul wirklich aktualisieren Erneut nach verfügbaren WLAN suchen Setup Abgeschlossen mirr.OS versucht jetzt, sich mit deinem WLAN zu verbinden. Du bekommst gleich eine E-Mail mit den Links zur weiteren Konfiguration.<br/>Das interne WLAN GlancrAP wird jetzt abgeschaltet. Verbinde dich wieder mit deinem Heim-WLAN.<br/>Bitte achte auf die Ausgabe auf deinem smart mirror. Setup Schritt 1 Setup Schritt 2 Entschuldigung Step 1 von 2 Step 2 von 2 Es gab einen Server-Fehler:  Sichtbare SSID Dein Vorname Gib Deine Stadt ein Verrate uns mehr über dich Wifi Bitte wähle aus der folgenden Liste dein WLAN aus und gib dein Passwort ein. Bitte verbinde dein Smartphone oder Notebook nochmal mit dem WLAN GlancrAP und überprüfe deine WLAN-Daten. Deine E-Mail Adresse 